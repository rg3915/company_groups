# company_groups

Django Test relation between Company and Group.

As empresas possuem grupos de usuários.


## Problema

Cada empresa pode ter vários grupos, e mesmo que esses grupos tenham o mesmo nome, mas tenham permissões diferentes...

## Solução

Criar grupos para cada empresa com o nome da empresa junto com o nome do grupo. Exemplo:

```
# Group.name
empresa1-grupo1
empresa2-grupo1
empresa2-grupo2
```

Reparem que em `create_data.py` cada empresa tem seus grupos. E o script "escolhe", para cada empresa, apenas os grupos da própria empresa.


## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

```
git clone https://gitlab.com/rg3915/company_groups.git
cd company_groups
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python contrib/env_gen.py
python manage.py migrate
python create_data.py
```

Veja o painel de Admin.