from django.test import TestCase
from myproject.core.models import Company


class CompanyTest(TestCase):

    def test_should_return_attributes(self):
        fields = (
            'cnpj',
            'name',
            'status',
            'initials',
            'maxUsers',
            'groups',
        )

        for field in fields:
            with self.subTest():
                self.assertTrue(hasattr(Company, field))
