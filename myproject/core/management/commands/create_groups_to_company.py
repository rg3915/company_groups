from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from myproject.core.models import Company


GROUPS = [
    'director',
    'manager',
    'employee',
    'simply',
    'dummy',
]


def create_groups(company):
    for group in GROUPS:
        name = '{}-{}'.format(company.cnpj, group)
        Group.objects.get_or_create(name=name)


def add_groups(company):
    '''
    Adiciona os grupos a empresa.
    '''
    groups = Group.objects.filter(name__startswith=company.cnpj)
    print(groups)
    for group in groups:
        company.groups.add(group)


class Command(BaseCommand):
    help = ''' Cria os grupos e associa a empresa. '''

    def add_arguments(self, parser):
        parser.add_argument('-c', '--company', type=str,
                            help='CNPJ da empresa.')

    def handle(self, company, *args, **kwargs):
        company = Company.objects.get(cnpj=company)
        create_groups(company)
        add_groups(company)
