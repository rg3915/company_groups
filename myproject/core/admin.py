from django.contrib import admin
from .models import Company


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('cnpj', 'name', 'initials',  'custom_groups')
    list_filter = ('groups',)
    search_fields = ('cnpj', 'name')

    def custom_groups(self, obj):
        if obj.groups:
            """
            Get group, separate by comma,
            and display empty string if user has no group.
            """
            if obj.groups.count():
                groups_list = [group.name for group in obj.groups.all()]
                return ', '.join(groups_list)
            return ''

    custom_groups.short_description = 'grupos'
