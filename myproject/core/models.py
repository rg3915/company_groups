from django.db import models
from django.contrib.auth.models import Group


class Company(models.Model):
    cnpj = models.CharField(max_length=14, unique=True)
    name = models.CharField(max_length=250, null=False)
    status = models.IntegerField(default=1)
    initials = models.CharField(max_length=10, null=False, default='LEGALBOT')
    maxUsers = models.IntegerField(default=25)
    groups = models.ManyToManyField(
        Group,
        related_name='company_groups',
        blank=True
    )

    class Meta:
        ordering = ('name',)
        verbose_name = 'company'
        verbose_name_plural = 'companies'

    def __str__(self):
        return '{}-{}'.format(self.cnpj, self.name)
