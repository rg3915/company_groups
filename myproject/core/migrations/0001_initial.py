# Generated by Django 2.2.3 on 2019-07-08 17:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cnpj', models.CharField(max_length=14, unique=True)),
                ('name', models.CharField(max_length=250)),
                ('status', models.IntegerField(default=1)),
                ('initials', models.CharField(default='LEGALBOT', max_length=10)),
                ('maxUsers', models.IntegerField(default=25)),
                ('groups', models.ManyToManyField(related_name='company_groups', to='auth.Group')),
            ],
            options={
                'verbose_name': 'company',
                'verbose_name_plural': 'companies',
            },
        ),
    ]
