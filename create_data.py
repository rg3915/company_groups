import os
import django
import string
from random import choice, randint, sample

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myproject.settings")
django.setup()

from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
from myproject.core.models import Company


GROUPS = [
    'director',
    'manager',
    'employee',
    'simply',
    'dummy',
]

COMPANIES = [
    {
        'name': 'Beverly Stephenson',
        'initials': 'BS',
    },
    {
        'name': 'Chadwick Robbins',
        'initials': 'CR',
    },
    {
        'name': 'Blythe Park',
        'initials': 'BP',
    },
    {
        'name': 'Vance Wooten',
        'initials': 'VW',
    },
]

PERMISSIONS = [
    {
        'name': 'Can add something',
        'codename': 'add_something',
    },
    {
        'name': 'Can change something',
        'codename': 'change_something',
    },
    {
        'name': 'Can import something',
        'codename': 'import_something',
    },
    {
        'name': 'Can export something',
        'codename': 'export_something',
    },
]


def gen_digits(max_length):
    return str(''.join(choice(string.digits) for i in range(max_length)))


def create_groups():
    aux = []
    for company in COMPANIES:
        for item in GROUPS:
            name = '{} {}'.format(company['initials'], item)
            obj = Group(name=name)
            aux.append(obj)
    # Deleta Group, caso exista.
    groups = Group.objects.all()
    if groups.count():
        groups.delete()
    Group.objects.bulk_create(aux)


def choice_groups(initials: str):
    '''
    Escolhe alguns grupos aleatórios.
    '''
    groups = Group.objects.filter(name__startswith=initials)
    groups = groups.values_list('pk', flat=True)
    max_value = randint(1, 4)
    return sample(list(groups), k=max_value)


def create_permissions(model):
    '''
    Cria mais algumas permissões.
    '''
    # Deleta as permissões
    permissions = [item['codename'] for item in PERMISSIONS]
    Permission.objects.filter(codename__in=permissions).delete()
    # Add
    content_type = ContentType.objects.get_for_model(model=model)
    Permission.objects.filter()
    aux = []
    for item in PERMISSIONS:
        obj = Permission(
            content_type=content_type,
            name=item['name'],
            codename=item['codename'],
        )
        # O ideal seria usar um get_or_create()
        aux.append(obj)
    Permission.objects.bulk_create(aux)


def choice_permissions(model):
    '''
    Escolhe algumas permissões aleatórias.
    '''
    content_type = ContentType.objects.get_for_model(model=model)
    permissions = Permission.objects.filter(content_type=content_type)
    permissions = permissions.values_list('pk', flat=True)
    max_value = randint(2, 4)
    return sample(list(permissions), k=max_value)


def add_permissions():
    '''
    Adiciona permissões para todos os grupos.
    '''
    groups = Group.objects.all()
    for group in groups:
        sample_perms = choice_permissions(Company)
        permissions = Permission.objects.filter(pk__in=sample_perms)
        for permission in permissions:
            group.permissions.add(permission)


def create_companies():
    aux = []
    for item in COMPANIES:
        cnpj = gen_digits(14)
        obj = Company(
            cnpj=cnpj,
            name=item['name'],
            initials=item['initials'],
        )
        aux.append(obj)
    # Deleta Company, caso exista.
    companies = Company.objects.all()
    if companies.count():
        companies.delete()
    company = Company.objects.bulk_create(aux)
    # Add group
    for company in companies:
        sample_groups = choice_groups(company.initials)
        groups = Group.objects.filter(pk__in=sample_groups)
        for group in groups:
            company.groups.add(group)


if __name__ == '__main__':
    create_groups()
    create_companies()
    create_permissions(Company)
    add_permissions()
